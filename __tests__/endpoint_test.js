/*jshint esversion: 8 */
require("whatwg-fetch");
require("../src/endpoint.js");

test('get endpoint', () => {
	const endpoint = window.endpoint.get("/");
	expect(!!endpoint).toEqual(true);
	expect(endpoint).toEqual(window.endpoint.get("/"));
	const endpoint2 = window.endpoint.get("/r2");
	expect(!!endpoint2).toEqual(true);
	expect(endpoint===endpoint2).toEqual(false);
	const bad_endpoint = window.endpoint.get("adfs");
	expect(bad_endpoint).toEqual(null);
	const g1 = window.endpoint.get("//test.com/blabla");
	const g2 = window.endpoint.get("http://test.com/blabla");
	const g3 = window.endpoint.get("https://test.com/blabla");
	const g4 = window.endpoint.get("https://test.com/blabl");
	expect(!!g1).toEqual(true);
	expect(!!g2).toEqual(true);
	expect(!!g3).toEqual(true);
	expect(!!g4).toEqual(true);
	expect(g1===g2).toEqual(true);
	expect(g1===g3).toEqual(true);
	expect(g1===g4).toEqual(false);
});

test('get endpoint regex', () => {
	const classic_endpoint = window.endpoint.get("/test/");
	const endpoint = window.endpoint.get(/test/);

	expect(!!endpoint).toEqual(true);
	expect(!!classic_endpoint).toEqual(true);
	expect(endpoint===classic_endpoint).toEqual(false);
	expect(endpoint===window.endpoint.get(/test/)).toEqual(true);
	expect(endpoint===window.endpoint.get(/test2/)).toEqual(false);
});

test('event trigger', () => {
	const endpoint = window.endpoint.get("/");
	let called = 0;
	const callback = () => {called++;};
	endpoint.addEventListener("load", callback);
	endpoint.addEventListener("error", callback);
	endpoint.addEventListener("load", callback, "POST");
	endpoint.load("GET");
	endpoint.error("error");
	expect(called).toEqual(2);
	endpoint.load("POST");
	endpoint.addEventListener("load", callback);
	endpoint.removeEventListener("load", callback);
	endpoint.removeEventListener("load", callback, "POST");
	var fall = false;
	try {
		endpoint.removeEventListener("lofdsad", callback, "POST");
	}
	catch (e) {fall = true;}
	expect(fall).toEqual(true);
	endpoint.load("POST");
	expect(called).toEqual(3);

	fall = 0;
	try {
		endpoint.removeEventListener("lojlkjsilaad", callback);
	} catch(e) { fall++; }
	expect(fall).toEqual(1);

	try {
		endpoint.removeEventListener("load", callback, "PIVO");
	} catch(e) { fall++; }
	expect(fall).toEqual(1);


	endpoint.load("GET");
	endpoint.load("POST");
	expect(called).toEqual(3);
});

test('terminate endpoint', () => {
	let endpoint = window.endpoint.get("/");
	const callback = () => {called++;};
	let called = 0;
	endpoint.addEventListener("load", callback);
	endpoint.load("GET");
	expect(called).toEqual(1);
	window.endpoint.terminate("/");
	endpoint = window.endpoint.get("/");
	endpoint.load("GET");
	expect(called).toEqual(1);
	endpoint.addEventListener("load", callback);
	endpoint.load("GET");
	expect(called).toEqual(2);
	window.endpoint.terminate(endpoint);
	endpoint = window.endpoint.get("/");
	endpoint.load("GET");
	expect(called).toEqual(2);
});

test('terminate endpoint regex', () => {
	let endpoint = window.endpoint.get(/test/);
	const callback = () => {called++;};
	let called = 0;
	endpoint.addEventListener("load", callback);
	endpoint.load("GET");
	expect(called).toEqual(1);
	window.endpoint.terminate(/test/);
	endpoint = window.endpoint.get(/test/);
	endpoint.load("GET");
	expect(called).toEqual(1);
	endpoint.addEventListener("load", callback);
	endpoint.load("GET");
	expect(called).toEqual(2);
	window.endpoint.terminate(endpoint);
	endpoint = window.endpoint.get(/test/);
	endpoint.load("GET");
	expect(called).toEqual(2);
});

test('trigger', async () => {
	const url = "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false";
	window.endpoint.terminate(url);
	let endpoint = window.endpoint.get(url);
	const callback = () => {called++;};
	let called = 0;
	let error = 0;
	endpoint.addEventListener("load", callback);

	expect(called).toEqual(0);

	let xhr = new XMLHttpRequest();
	xhr.open("GET", url, true);
	xhr.send();

	await ((ms) => {
		return new Promise(resolve => setTimeout(resolve, ms));
	})(500);
	expect(called).toEqual(1);

	xhr = new XMLHttpRequest();
	xhr.open("GET", url, true, null, null, true);
	xhr.send();

	await ((ms) => {
		return new Promise(resolve => setTimeout(resolve, ms));
	})(500);
	expect(called).toEqual(1);


	await fetch(url);
	await ((ms) => {
		return new Promise(resolve => setTimeout(resolve, ms));
	})(500);
	expect(called).toEqual(3); // fetch is a polyfill of XMLHttpRequest so its trigered 2xs

	await fetch(url, {}, true);
	await ((ms) => {
		return new Promise(resolve => setTimeout(resolve, ms));
	})(500);
	expect(called).toEqual(4); // increased by 1 (polyfill)
});

test('trigger regex', async () => {
	const url = "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false";
	const error_url = "https://api.coingecko.com/api/v3/co";
	window.endpoint.terminate(url);
	let endpoint = window.endpoint.get(/https:\/\/api\.coingecko\.com\/api\/.*/i);
	let called = 0;
	let error = 0;
	const callback = () => {called++;};
	const callback_error = () => {error++;};
	endpoint.addEventListener("load", callback);
	endpoint.addEventListener("error", callback_error);

	expect(called).toEqual(0);

	let xhr = new XMLHttpRequest();
	xhr.open("GET", url, true);
	xhr.send();

	await ((ms) => {
		return new Promise(resolve => setTimeout(resolve, ms));
	})(500);
	expect(called).toEqual(1);

	xhr = new XMLHttpRequest();
	xhr.open("GET", url, true, null, null, true);
	xhr.send();

	await ((ms) => {
		return new Promise(resolve => setTimeout(resolve, ms));
	})(500);
	expect(called).toEqual(1);


	await fetch(url);
	await ((ms) => {
		return new Promise(resolve => setTimeout(resolve, ms));
	})(500);
	expect(called).toEqual(3); // fetch is a polyfill of XMLHttpRequest so its trigered 2xs

	await fetch(url, {}, true);
	await ((ms) => {
		return new Promise(resolve => setTimeout(resolve, ms));
	})(500);
	expect(called).toEqual(4); // increased by 1 (polyfill)


	xhr = new XMLHttpRequest();
	xhr.open("GET", error_url, true);
	xhr.send();

	await ((ms) => {
		return new Promise(resolve => setTimeout(resolve, ms));
	})(500);
	expect(error).toEqual(1);

	await fetch(error_url);
	await ((ms) => {
		return new Promise(resolve => setTimeout(resolve, ms));
	})(500);
	expect(error).toEqual(3); // increased by 2 (polyfill)
});
