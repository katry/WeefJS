/*jshint esversion: 8 */

Object.defineProperty(Object, "deepCopy", {
	value: (input) => {
		if (input === null || (!(input instanceof Array) && input.constructor !== Object))
			return input;
		const copy = Array.isArray(input) ? [] : {};
		for (let key in input)
			copy[key] = Object.deepCopy(input[key]);
		return copy;
	},
	enumerable: false,
	configurable: false,
	writable: false
});

Object.defineProperty(HTMLElement.prototype, "state", {
	get() {
		if(!this._state)
			this._state = {};
		return this._state;
	}
});

(() => {
	const init = () => {
	const observe = (nodes) => {
		nodes.forEach(node => {
			if(node.nodeName[0] === "#") return;
			if((node instanceof HTMLAnchorElement || node instanceof HTMLButtonElement) && !node.OBSERVED) {
				node.addEventListener("click", function(e) {
					const href = this.getAttribute("href") || this.dataset.href || "";
					if(href && !/^((\/\/)|(https?:\/\/)).+/i.test(href)) {
						const state = Object.deepCopy(this.state);
						const title = this.title || this.dataset.title;
						if(!state.title && title)
							state.title = title;
						history.pushState(state, "", href);
						e.preventDefault();
					}
				});
			}
			else if(node.nodeName[0] !== "#" && node.nodeName !== "svg" && !node.OBSERVED) {
				node.childNodes.forEach(n => {
						observe(node.childNodes);
				});
				(new MutationObserver(m => m.forEach(m => observe(m.addedNodes)))).observe(node, {childList: true, subtree: false});
			}
			Object.defineProperty(node, "OBSERVED", {
					value: true,
					enumerable: false,
					configurable: false,
					writable: false
			});
		});
	};
	observe([document.body]);
};
if(document.body)
	init();
else window.addEventListener("DOMContentLoaded", init);
})();
