/*jshint esversion: 8 */

window.route = new (class {
	#regexRoutes = {};
	#routes = {};
	#active = [];

	constructor() {
		const _ = (_) => {
			return (state, unused, url, skip=false) => {
				const return_value = _.call(history, state, unused, url);
				if(url && !skip)
					this.load(state);
				return return_value;
			};
		};
		history.pushState = _(history.pushState);
		history.replaceState = _(history.replaceState);
		addEventListener('popstate', e => { this.load(e.state); });
	}

	load(_state=null) {
		let path = location.pathname.toLowerCase();
		if(!path.endsWith("/"))
			path += "/";
		const routes = Object.values(this.#regexRoutes).filter(e => e.pattern.test(path));
		if(this.#routes[path])
			routes.unshift(this.#routes[path]);
		this.#active.forEach(route => route.unload());
		this.#active = [];
		routes.forEach(route => {
			route.load(_state);
			this.#active.push(route);
		});
	}

	get(pattern) {
		const input = this.#validate(pattern);
		return input[2][input[1]] = input[2][input[1]] || new (class {
			#pattern;
			#listeners = {load: [], beforeunload: []};

			constructor(pattern) {
				this.#pattern = pattern;
			}

			addEventListener(type, callback) {
				if(typeof callback !== "function")
					throw new TypeError("Invalid callback");
				if(!this.#listeners[type])
					throw new TypeError(`Invalid listener ${type}`);
				this.#listeners[type].push(callback);
			}

			removeEventListener(type, callback) {
				if(!this.#listeners[type])
					throw new TypeError( `Invalid listener ${type}`);
				this.#listeners[type] = this.#listeners[type].filter(e => e!==callback);
			}

			load(state=null) {
				this.#listeners.load.forEach(e => e(state, this));
			}

			unload() {
				this.#listeners.beforeunload.forEach(e => e(this));
			}

			get pattern() { return this.#pattern }
		})(input[0]);
	}

	terminate(rop) {
		const input = this.#validate(rop?.pattern || rop);
		delete(input[2][input[1]]);
	}

	#validate(pattern) {
		if(typeof pattern === "string")
			pattern = (pattern.endsWith("/") ? pattern : pattern + "/").toLowerCase();
		const key = pattern.toString().toLowerCase();
		if(key[0] !== "/")
			throw new TypeError(`Invalid route ${key}`);
		return [pattern, key, pattern instanceof RegExp ? this.#regexRoutes : this.#routes];
	}
})();
