/*jshint esversion: 8 */

Object.defineProperty(Object, "deepCopy", {
	value: (input) => {
		if (input === null || (!(input instanceof Array) && input.constructor !== Object))
			return input;
		const copy = Array.isArray(input) ? [] : {};
		for (let key in input)
			copy[key] = Object.deepCopy(input[key]);
		return copy;
	},
	enumerable: false,
	configurable: false,
	writable: false
});

Object.defineProperty(HTMLElement.prototype, "state", {
	get() {
		if(!this._state)
			this._state = {};
		return this._state;
	}
});

(() => {
	const init = () => {
	const observe = (nodes) => {
		nodes.forEach(node => {
			if(node.nodeName[0] === "#") return;
			if((node instanceof HTMLAnchorElement || node instanceof HTMLButtonElement) && !node.OBSERVED) {
				node.addEventListener("click", function(e) {
					const href = this.getAttribute("href") || this.dataset.href || "";
					if(href && !/^((\/\/)|(https?:\/\/)).+/i.test(href)) {
						const state = Object.deepCopy(this.state);
						const title = this.title || this.dataset.title;
						if(!state.title && title)
							state.title = title;
						history.pushState(state, "", href);
						e.preventDefault();
					}
				});
			}
			else if(node.nodeName[0] !== "#" && node.nodeName !== "svg" && !node.OBSERVED) {
				node.childNodes.forEach(n => {
						observe(node.childNodes);
				});
				(new MutationObserver(m => m.forEach(m => observe(m.addedNodes)))).observe(node, {childList: true, subtree: false});
			}
			Object.defineProperty(node, "OBSERVED", {
					value: true,
					enumerable: false,
					configurable: false,
					writable: false
			});
		});
	};
	observe([document.body]);
};
if(document.body)
	init();
else window.addEventListener("DOMContentLoaded", init);
})();

/*jshint esversion: 8 */

window.endpoint = new class {
	#regexEndpoints = {};
	#endpoints = {};

	constructor() {
		const _fetch = fetch;
		const _XMLHttpRequestOpen = XMLHttpRequest.prototype.open;
		window.fetch = (req, opt={}, skip=false) => {
			const url = req instanceof Request ? req.url : req;
			const endpoints = this.#getEndpoints(url);
			if(!skip && endpoints.length) {
				return _fetch(req, opt).then(res => {
					const clone = res.clone();
					if(res.status > 399)
						for(let i in endpoints)
							endpoints[i].error(url, res.status, res.statusText);
					else res.text().then(data => {
						try { data = JSON.parse(data); }
						catch(e) {}
						for(let i in endpoints)
							endpoints[i].load(req.method || opt.method || "GET", data);
					});
					return clone;
				});
			}
			else return _fetch(req, opt);
		};

		XMLHttpRequest.prototype.open = ((context) => {
			return function(method, url, async, user, password, skip) {
				const r = _XMLHttpRequestOpen.apply(this, [].slice.call(arguments));
				const endpoints = context.#getEndpoints(url);
				if(!skip && endpoints.length) {
					this.addEventListener("readystatechange", (evt) => {
						if(this.readyState === XMLHttpRequest.DONE) {
							if(this.status > 399) {
								for(let i in endpoints)
									endpoints[i].error(url, this.status, this.statusText);
								return;
							}
							let data = evt.responseText;
							try {data = JSON.parse(data);}
							catch(e) {}
							for(let i in endpoints)
								endpoints[i].load(method, data);
						}
					});
				}
				return r;
			};
		})(this);
	}

	get(pattern) {
		const input = this.#validate(pattern);
		return input.length ? input[2][input[1]] = input[2][input[1]] || new (class {
			#pattern;
			#listeners = {load: {}, error: []};

			constructor(pattern) {
				this.#pattern = pattern;
			}

			addEventListener(type, callback, method="GET") {
				if(typeof callback !== "function")
					throw new TypeError("Invalid callback");
				if(!this.#listeners[type])
					throw new TypeError(`Invalid listener ${type}`);
				if(type === "load") {
					if(!this.#listeners[type][method])
						this.#listeners[type][method] = [];
					this.#listeners[type][method].push(callback);
				}
				else this.#listeners[type].push(callback);
			}

			removeEventListener(type, callback, method="GET") {
				if(!this.#listeners[type])
					throw new TypeError( `Invalid listener ${type}`);
				if(type === "load")
					this.#listeners[type][method] = (this.#listeners[type][method] || []).forEach(e => e!==callback);
				else this.#listeners[type] = this.#listeners[type].filter(e => e!==callback);
			}

			load(method, data) {
				(this.#listeners.load[method] || []).forEach(i => i(data));
			}

			error(url, status, statusText="") {
				this.#listeners.error.forEach(i => i(url, status, statusText));
			}

			get pattern() { return this.#pattern; }
		})(pattern) : null;
	}

	terminate(rop) {
		const input = this.#validate(rop?.pattern || rop);
		delete(input[2][input[1]]);
	}

	#validate(pattern) {
		if(typeof pattern === "string")
			pattern = (pattern.endsWith("/") ? pattern : pattern + "/").toLowerCase();
		const key = pattern.toString().toLowerCase();
		if(/^((\/\/?)|(https?:\/\/)).*$/i.test(key))
			return [pattern, key.replace(/^https?:/i, ""), pattern instanceof RegExp ? this.#regexEndpoints : this.#endpoints];
		return [];
	}

	#getEndpoints(url) {
		const endpoints = Object.values(this.#regexEndpoints).filter(e => e.pattern.test(url));
		if(url = this.#endpoints[(url.endsWith("/") ? url : url + '/').replace(/^https?:/i, "")])
			endpoints.unshift(url);
		return endpoints;
	}
}

/*jshint esversion: 8 */

window.route = new (class {
	#regexRoutes = {};
	#routes = {};
	#active = [];

	constructor() {
		const _ = (_) => {
			return (state, unused, url, skip=false) => {
				const return_value = _.call(history, state, unused, url);
				if(url && !skip)
					this.load(state);
				return return_value;
			};
		};
		history.pushState = _(history.pushState);
		history.replaceState = _(history.replaceState);
		addEventListener('popstate', e => { this.load(e.state); });
	}

	load(_state=null) {
		let path = location.pathname.toLowerCase();
		if(!path.endsWith("/"))
			path += "/";
		const routes = Object.values(this.#regexRoutes).filter(e => e.pattern.test(path));
		if(this.#routes[path])
			routes.unshift(this.#routes[path]);
		this.#active.forEach(route => route.unload());
		this.#active = [];
		routes.forEach(route => {
			route.load(_state);
			this.#active.push(route);
		});
	}

	get(pattern) {
		const input = this.#validate(pattern);
		return input[2][input[1]] = input[2][input[1]] || new (class {
			#pattern;
			#listeners = {load: [], beforeunload: []};

			constructor(pattern) {
				this.#pattern = pattern;
			}

			addEventListener(type, callback) {
				if(typeof callback !== "function")
					throw new TypeError("Invalid callback");
				if(!this.#listeners[type])
					throw new TypeError(`Invalid listener ${type}`);
				this.#listeners[type].push(callback);
			}

			removeEventListener(type, callback) {
				if(!this.#listeners[type])
					throw new TypeError( `Invalid listener ${type}`);
				this.#listeners[type] = this.#listeners[type].filter(e => e!==callback);
			}

			load(state=null) {
				this.#listeners.load.forEach(e => e(state, this));
			}

			unload() {
				this.#listeners.beforeunload.forEach(e => e(this));
			}

			get pattern() { return this.#pattern }
		})(input[0]);
	}

	terminate(rop) {
		const input = this.#validate(rop?.pattern || rop);
		delete(input[2][input[1]]);
	}

	#validate(pattern) {
		if(typeof pattern === "string")
			pattern = (pattern.endsWith("/") ? pattern : pattern + "/").toLowerCase();
		const key = pattern.toString().toLowerCase();
		if(key[0] !== "/")
			throw new TypeError(`Invalid route ${key}`);
		return [pattern, key, pattern instanceof RegExp ? this.#regexRoutes : this.#routes];
	}
})();
