# CHANGELOG

## 1.1.1 (2022-08-15)
- fetch endpoint chaining fixed

## 1.1.0 (2022-08-15)
- route pushState and replaceState skip argument implementation

## 1.0.2 (2022-08-15)
- fixed observer for nodes created directly by innerHTML

## 1.0.1 (2022-05-21)

- example and readme fixed
- added file list to package.json

## 1.0.0 (2022-05-21)

- jest tests
- first working release

